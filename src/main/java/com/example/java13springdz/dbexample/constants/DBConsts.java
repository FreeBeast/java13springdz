package com.example.java13springdz.dbexample.constants;

public interface DBConsts {
    String DB_HOST = "localhost";
    String DB = "local_dz";
    String USER = "postgres_dz";
    String PASSWORD = "12345";
    String PORT = "5432";
}

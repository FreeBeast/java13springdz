package com.example.java13springdz.dbexample.dao;

import org.springframework.stereotype.Component;

import java.sql.Connection;


@Component
public class BookDaoBean {

    private final Connection connection;

    public BookDaoBean(Connection connection) {
        this.connection = connection;
    }

}

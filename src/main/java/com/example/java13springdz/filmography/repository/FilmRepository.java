package com.example.java13springdz.filmography.repository;

import com.example.java13springdz.filmography.model.Film;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends GenericRepository<Film>{
}

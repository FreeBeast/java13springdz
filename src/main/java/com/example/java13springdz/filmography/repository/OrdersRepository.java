package com.example.java13springdz.filmography.repository;

import com.example.java13springdz.filmography.model.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRepository extends GenericRepository<Order>{
}

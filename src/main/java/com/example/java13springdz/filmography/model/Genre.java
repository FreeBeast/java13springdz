package com.example.java13springdz.filmography.model;

public enum Genre {

    FANTASY("Фантастика"),
    DRAMA("Драма"),
    SCIENCE_FICTION("Научная фантастика"),
    NOVEL("Роман");

    private final String genreDisplayValue;

    Genre(String text) {
        this.genreDisplayValue = text;
    }

    public String getGenreDisplayValue() {
        return this.genreDisplayValue;
    }
}

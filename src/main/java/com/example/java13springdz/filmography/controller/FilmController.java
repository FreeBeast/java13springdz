package com.example.java13springdz.filmography.controller;

import com.example.java13springdz.filmography.model.Directors;
import com.example.java13springdz.filmography.model.Film;
import com.example.java13springdz.filmography.repository.DirectorsRepository;
import com.example.java13springdz.filmography.repository.FilmRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;

@RestController
@RequestMapping(value = "/films")
@Tag(name = "Фильмы",
        description = "Контроллер для работы с фильмами фильмотеки")
public class FilmController extends GenericController<Film> {
    private final FilmRepository filmRepository;
    private final DirectorsRepository directorsRepository;

    public FilmController(FilmRepository filmRepository,DirectorsRepository directorsRepository){
        super(filmRepository);
        this.filmRepository=filmRepository;
        this.directorsRepository=directorsRepository;
    }
    @Operation(description = "Добавить директора к фильму", method = "addDirectors")
    @RequestMapping(value = "/addDirectors", method = RequestMethod.POST
            , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Film> addDirectors(@RequestParam(value = "filmId") Long filmId
            , @RequestParam(value = "directorsId") Long directorsId) {
        Film film = filmRepository.findById(filmId).orElseThrow(()-> new NotFoundException("Фильм не найден"));
        Directors directors = directorsRepository.findById(directorsId).orElseThrow(()-> new NotFoundException("Директор не найден"));
        film.getDirectors().add(directors);
        return ResponseEntity.status(HttpStatus.OK).body(filmRepository.save(film));

    }

}

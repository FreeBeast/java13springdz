package com.example.java13springdz.filmography.controller;

import com.example.java13springdz.filmography.model.Order;
import com.example.java13springdz.filmography.repository.OrdersRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/orders")
@Tag(name = "Заказы",
        description = "Контроллер для работы с заказами фильмотеки")
public class OrdersController extends GenericController<Order>{
    private final OrdersRepository ordersRepository;

    public OrdersController(OrdersRepository ordersRepository){
        super(ordersRepository);
        this.ordersRepository=ordersRepository;
    }
}

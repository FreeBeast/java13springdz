package com.example.java13springdz.filmography.controller;

import com.example.java13springdz.filmography.model.Users;
import com.example.java13springdz.filmography.repository.UsersRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями фильмотеки")
public class UsersController extends GenericController<Users>{
    private final UsersRepository usersRepository;
    public UsersController(UsersRepository usersRepository){
        super(usersRepository);
        this.usersRepository=usersRepository;
    }
}

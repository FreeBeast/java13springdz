package com.example.java13springdz.filmography.controller;

import com.example.java13springdz.filmography.model.Directors;
import com.example.java13springdz.filmography.model.Film;
import com.example.java13springdz.filmography.repository.DirectorsRepository;
import com.example.java13springdz.filmography.repository.FilmRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;

import java.util.Set;

@RestController
@RequestMapping(value = "/directors")
@Tag(name = "Директора",
        description = "Контроллер для работы с директорами фильмотеки")
public class DirectorsController extends GenericController<Directors>{
    private final DirectorsRepository directorsRepository;
    private final FilmRepository filmRepository;

    public DirectorsController(DirectorsRepository directorsRepository,
                               FilmRepository filmRepository){
        super(directorsRepository);
        this.directorsRepository=directorsRepository;
        this.filmRepository = filmRepository;
    }

    @Operation(description = "Добавить фильм к директору", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST
            , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Directors> addFilm(@RequestParam(value = "filmId") Long filmId
            , @RequestParam(value = "directorsId") Long directorsId) {
        Film film = filmRepository.findById(filmId).orElseThrow(()-> new NotFoundException("Фильм не найден"));
        Directors directors = directorsRepository.findById(directorsId).orElseThrow(()-> new NotFoundException("Директор не найден"));
        directors.getFilms().add(film);
        return ResponseEntity.status(HttpStatus.OK).body(directorsRepository.save(directors));

    }

}
